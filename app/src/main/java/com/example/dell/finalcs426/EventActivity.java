package com.example.dell.finalcs426;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.finalcs426.Model.CheckInEvent;
import com.example.dell.finalcs426.Model.Event;
import com.example.dell.finalcs426.Model.MusicEvent;
import com.example.dell.finalcs426.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class EventActivity extends AppCompatActivity {
    String eventKey;
    Intent intent;
    TextView title,coin,exp,description;
    Button start;
    ImageView image,star;
    ProgressBar progressBar;
    Context context;
    FirebaseUser firebaseUser;
    String userID;
    boolean completed=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_event);
        intent=this.getIntent();
        eventKey=intent.getStringExtra("event_key");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences preferences   = getSharedPreferences("PRESS", Context.MODE_PRIVATE);
        userID = preferences.getString("userID","none");

        checkEventCompleted();

        title=findViewById(R.id.event_activity_title);
        coin=findViewById(R.id.activity_event_coin);
        exp=findViewById(R.id.activity_event_exp);
        image=findViewById(R.id.activity_event_image);
        description=findViewById(R.id.activity_event_description);
        star=findViewById(R.id.activity_event_star);
        progressBar=findViewById(R.id.event_activity);
        start=findViewById(R.id.start_event);

        eventData();

    }

    private void checkEventCompleted() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("CompletedEvent").child(eventKey).child(userID);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null&&(boolean)dataSnapshot.getValue())
                {
                    star.setImageResource(R.drawable.yellow_star);
                    completed=true;
                    progressBar.setProgress(100);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void eventData() {
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Events").child("CheckIn").child(eventKey);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final CheckInEvent e=dataSnapshot.getValue(CheckInEvent.class);

                if(e!=null) {
                    title.setText(e.getTitle());
                    e.setKey(eventKey);
                    coin.setText(e.getCoin()+" coins");
                    description.setText(e.getDescription());
                    exp.setText(e.getExp()+ "exp");
                    Picasso.with(context)
                            .load(e.getImageUrl())
                            .into(image);
                    start.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            e.doEvent(context);
                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("Events").child("Music").child(eventKey);
        reference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final MusicEvent e=dataSnapshot.getValue(MusicEvent.class);

                if(e!=null)
                {
                    e.setKey(eventKey);
                    title.setText(e.getTitle());
                    coin.setText("Coins: "+e.getCoin());
                    description.setText("Description: "+e.getDescription());
                    exp.setText("Exp: "+e.getExp());
                    Picasso.with(context)
                            .load(e.getImageUrl())
                            .into(image);
                    start.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            e.doEvent(context);
                            if(!completed) {
                                completed = true;
                                progressBar.setProgress(100);
                                star.setImageResource(R.drawable.yellow_star);

                                //Check isEvent
                                DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference()
                                        .child("Events").child("CheckIn");
                                eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChild(eventKey) )
                                        {
                                            //Update to firebase
                                            FirebaseDatabase.getInstance().getReference()
                                                    .child("CompletedEvent").child(eventKey)
                                                    .child(firebaseUser.getUid()).setValue(true);
                                            // Update User coin & exp
                                            final int[] coin = {0};
                                            final int[] exp = {0};
                                            //get current user's coin,exp

                                            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                                    .child("Users").child(firebaseUser.getUid());
                                            ref.addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    User user = dataSnapshot.getValue(User.class);
                                                    coin[0] += user.getCoin();
                                                    exp[0] += user.getExp();
                                                    user.updateLevel();
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                            //add up coin,exp with mission reward
                                            FirebaseDatabase.getInstance().getReference()
                                                    .child("Events").child("CheckIn").child(eventKey)
                                                    .addValueEventListener(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            CheckInEvent event = dataSnapshot.getValue(CheckInEvent.class);
                                                            coin[0] += event.getCoin();
                                                            exp[0] += event.getExp();
                                                            ref.child("coin").setValue(coin[0]);
                                                            ref.child("exp").setValue(exp[0]);
                                                            coin[0]=0;
                                                            exp[0]=0;
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                            Toast.makeText(context, "Congrats,"+"you has completed mission No."+eventKey, Toast.LENGTH_SHORT).show();
                                            // Move to event info

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(context, "Mission Failed !", Toast.LENGTH_SHORT).show();
                                    }
                                });



                            }
                            else{
                                Toast.makeText(context, "Mission Completed!", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
