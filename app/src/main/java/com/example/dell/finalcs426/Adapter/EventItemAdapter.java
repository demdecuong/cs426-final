package com.example.dell.finalcs426.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dell.finalcs426.EventActivity;
import com.example.dell.finalcs426.Model.Event;
import com.example.dell.finalcs426.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EventItemAdapter extends RecyclerView.Adapter<EventItemAdapter.MyViewHolder> {
    List<Event> events;
    Context context;
    FirebaseUser firebaseUser;
    String userID;

    public EventItemAdapter(List<Event> events, Context context) {
        this.events=events;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.event_item, viewGroup, false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences preferences   = context.getSharedPreferences("PRESS", Context.MODE_PRIVATE);
        userID = preferences.getString("userID","none");


        MyViewHolder viewHolder=new MyViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder v, final int i) {
        v.title.setText(events.get(i).getTitle());
        v.coin.setText(events.get(i).getCoin()+" coins");
        v.exp.setText(events.get(i).getExp()+" exps");
        Picasso.with(context)
                .load(events.get(i).getImageUrl())
                .into(v.image);
        v.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(context, EventActivity.class);
                intent.putExtra("event_key",events.get(i).getKey());
                context.startActivity(intent);

            }
        });

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("CompletedEvent").child(events.get(i).getKey()).child(userID);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null&&(boolean)dataSnapshot.getValue())
                {
                    v.star.setImageResource(R.drawable.yellow_star);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder{
        TextView title, coin,exp;
        ImageView image,star;
        RelativeLayout parentLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            coin=itemView.findViewById(R.id.coin);
            exp=itemView.findViewById(R.id.exp);
            image=itemView.findViewById(R.id.event_image);
            parentLayout=itemView.findViewById(R.id.event_item_layout);
            star=itemView.findViewById(R.id.completed_image);
        }
    }

}
