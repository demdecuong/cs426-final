package com.example.dell.finalcs426.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.finalcs426.Model.CheckInEvent;
import com.example.dell.finalcs426.Model.User;
import com.example.dell.finalcs426.R;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class ScanFragment extends Fragment {

    SurfaceView cameraPreview;
    TextView textView;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    final static String TAG = "ScanFragment";
    String scanResult;
    FirebaseUser firebaseUser;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        cameraSource.start(cameraPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scan, container, false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        cameraPreview = view.findViewById(R.id.camerapreview);
        textView = view.findViewById(R.id.txt_result);

        barcodeDetector = new BarcodeDetector.Builder(this.getContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this.getContext(), barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();
        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    // Request permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CAMERA},RequestCameraPermissionID);
                    return;
                }
                try {
                    cameraSource.start(cameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if(qrcodes.size()!=0)
                {
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            //Create vibrate
                            Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(250);
                            scanResult = qrcodes.valueAt(0).displayValue ;
                            textView.setText(qrcodes.valueAt(0).displayValue);
                            barcodeDetector.release(); //stop scanning
                            //Check isUser
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            ref.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.hasChild(scanResult) && !isFriendExist(scanResult)){
                                        // use "username" already exists
                                        //asking if user want to add this friend
                                        new AlertDialog.Builder(getContext())
                                                .setTitle("Add friend request")
                                                .setMessage("Are you sure you want to add "+ dataSnapshot.getKey()+" as a friend ?")

                                                // Specifying a listener allows you to take an action before dismissing the dialog.
                                                // The dialog is automatically dismissed when a dialog button is clicked.
                                                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // Continue with delete operation
                                                        // add friend
                                                        FirebaseDatabase.getInstance().getReference().child("Friends")
                                                                .child(firebaseUser.getUid()).child("friends").child(scanResult).setValue(true);
                                                        // update friend for added user
                                                        FirebaseDatabase.getInstance().getReference().child("Friends")
                                                                .child(scanResult).child("friends").child(firebaseUser.getUid()).setValue(true);

                                                        Toast.makeText(getContext(),scanResult+" has been your friend !", Toast.LENGTH_SHORT).show();
                                                    }
                                                })

                                                // A null listener allows the button to dismiss the dialog and take no further action.
                                                .setNegativeButton("NO", null)
                                                //.setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();


                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            //Check isEvent
                            DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference()
                                    .child("Events").child("CheckIn");
                            eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.hasChild(scanResult) && !isCompletedEventExist(scanResult))
                                    {
                                        //Update to firebase
                                        FirebaseDatabase.getInstance().getReference()
                                                .child("CompletedEvent").child(scanResult)
                                                .child(firebaseUser.getUid()).setValue(true);
                                        // Update User coin & exp
                                        final int[] coin = {0};
                                        final int[] exp = {0};
                                        //get current user's coin,exp
                                        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                                .child("Users").child(firebaseUser.getUid());
                                        ref.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                User user = dataSnapshot.getValue(User.class);
                                                coin[0] += user.getCoin();
                                                exp[0] += user.getExp();
                                                user.updateLevel();
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                        //add up coin,exp with mission reward
                                        FirebaseDatabase.getInstance().getReference()
                                                .child("Events").child("CheckIn").child(scanResult)
                                                .addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        CheckInEvent event = dataSnapshot.getValue(CheckInEvent.class);
                                                        coin[0] += event.getCoin();
                                                        exp[0] += event.getExp();
                                                        ref.child("coin").setValue(coin[0]);
                                                        ref.child("exp").setValue(exp[0]);
                                                        coin[0]=0;
                                                        exp[0]=0;
                                                    }
                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                        Toast.makeText(getContext(), "Congrats,"+"you has completed mission No."+scanResult, Toast.LENGTH_SHORT).show();
                                        // Move to event info
                                        
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Toast.makeText(getContext(), "Mission Failed !", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            }
        });

        return view;
    }

    private boolean isCompletedEventExist(final String scanResult) {
        final boolean[] isExist = {false};
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("CompletedEvent").child(scanResult);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(firebaseUser.getUid()))
                {
                    isExist[0] = true;
                    return;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                return;
            }
        });
        if(isExist[0] == true)return true;
        return false;
    }

    private boolean isFriendExist(final String scanResult) {
        final boolean[] isFriend = {false};
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("Friends").child(firebaseUser.getUid()).child("friends");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(scanResult))
                {
                    isFriend[0] = true;
                    return;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                return;
            }
        });
        if(isFriend[0] == true)return true;
        return false;
    }
}
