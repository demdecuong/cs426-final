package com.example.dell.finalcs426.Fragment;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.finalcs426.Model.User;
import com.example.dell.finalcs426.QRCodeActivity;
import com.example.dell.finalcs426.R;
import com.example.dell.finalcs426.RewardActivity;
import com.example.dell.finalcs426.StartActivity;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class ProfileFragment extends Fragment {
    TextView username,fullName,level,coin,completed_mission;
    ImageView options,image_profile;
    Button edit_profile;
    FirebaseUser firebaseUser;
    String userID;
    StorageReference storageReference;
    private static final int IMAGE_REQUEST = 9;
    private Uri imageUri;
    private StorageTask uploadTask;
    final int countCompletedEvent[] ={0};
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.profile_menu,menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.archievement_tag:
                Toast.makeText(getContext(), "Item Reward", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), RewardActivity.class));
                return true;
            case R.id.facebook_tag:
                Toast.makeText(getContext(), "This feature will update soon !", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.qrcode_tag:
                Intent intent = new Intent(getActivity(), QRCodeActivity.class);
                intent.putExtra("userID",userID);
                startActivity(intent);
                return true;
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent( getActivity(), StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                return true;
                default:
                    super.onOptionsItemSelected(item);

        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences preferences   = getContext().getSharedPreferences("PRESS",Context.MODE_PRIVATE);
        userID = preferences.getString("userID","none");
        storageReference = FirebaseStorage.getInstance().getReference("uploads");

        username = view.findViewById(R.id.username);
        fullName = view.findViewById(R.id.fullname);
        level = view.findViewById(R.id.level);
        coin = view.findViewById(R.id.coins);
        completed_mission = view.findViewById(R.id.completed_mission);
        image_profile = view.findViewById(R.id.image_profile);
        edit_profile = view.findViewById(R.id.edit_profile);
        getCompletedMission();
        userInfo();
        image_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "This feature will updated soon !", Toast.LENGTH_SHORT).show();
            }
        });
        return  view;
    }

    private void openImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMAGE_REQUEST);
    }

    private String getFileExtension(Uri uri)
    {
        ContentResolver contentResolver = getContext().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadImage()
    {
        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setMessage("Uploading");
        pd.show();

        if(imageUri!=null)
        {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis()
                    +"."+getFileExtension(imageUri));
            uploadTask = fileReference.putFile(imageUri);
            uploadTask.continueWith(new Continuation<UploadTask.TaskSnapshot,Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful())
                    {
                        throw task.getException();
                    }
                    return fileReference.getDownloadUrl();
                }

            }).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful())
                    {
                        Object downloadUri =  task.getResult();

                        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                                .child(firebaseUser.getUid());
                        HashMap<String,Object> hashMap = new HashMap<>();
                        hashMap.put("imageurl",downloadUri.toString());
                        reference.updateChildren(hashMap);

                        pd.dismiss();
                    }else
                    {
                        Toast.makeText(getContext(), "Uploading Failed !", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            });
        }else
        {
            Toast.makeText(getContext(), "No image is selected", Toast.LENGTH_SHORT).show();
        }
    }

    private void userInfo()
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(userID);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(getContext()==null)return;

                User user = dataSnapshot.getValue(User.class);

                username.setText(firebaseUser.getDisplayName());
                fullName.setText(firebaseUser.getDisplayName());
                coin.setText(String.valueOf(user.getCoin()));
                level.setText(String.valueOf(user.getLevel()));

                Picasso.with(getContext())
                        .load(firebaseUser.getPhotoUrl())
                        .into(image_profile);
                imageUri = firebaseUser.getPhotoUrl();
                completed_mission.setText(""+countCompletedEvent[0]);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getCompletedMission()
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("CompletedEvent");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              for(DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    for(DataSnapshot snapshot1 : snapshot.getChildren())
                    {
                        if(snapshot1.getKey().equals(firebaseUser.getUid()))
                            countCompletedEvent[0]++;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_REQUEST && data!=null && data.getData()!=null)
        {
            imageUri = data.getData();
            if(uploadTask != null && uploadTask.isInProgress())
            {
                Toast.makeText(getContext(), "Upload in Preprogress", Toast.LENGTH_SHORT).show();
            }else
            {
                uploadImage();
            }
        }
    }
}
