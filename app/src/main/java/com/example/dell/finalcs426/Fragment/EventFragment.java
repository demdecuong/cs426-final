package com.example.dell.finalcs426.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dell.finalcs426.Adapter.EventItemAdapter;
import com.example.dell.finalcs426.Model.CheckInEvent;
import com.example.dell.finalcs426.Model.Event;
import com.example.dell.finalcs426.Model.MusicEvent;
import com.example.dell.finalcs426.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.client.android.Intents;

import java.nio.file.StandardWatchEventKinds;
import java.util.ArrayList;
import java.util.List;

public class EventFragment extends Fragment {

    FirebaseUser firebaseUser;
    String userID;
    TextView t;
    List<Event> events= new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences preferences   = getContext().getSharedPreferences("PRESS",Context.MODE_PRIVATE);
        userID = preferences.getString("userID","none");
        //add data

        addMusicEvents();
        addCheckInEvents();
        //recycler
        recyclerView= view.findViewById(R.id.event_recycler_view);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager

        layoutManager = new LinearLayoutManager(container.getContext());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new EventItemAdapter(events, container.getContext());
        recyclerView.setAdapter(mAdapter);




        return view;
    }

    private void addCheckInEvents() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Events").child("CheckIn");

        ChildEventListener childEventListener=new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                CheckInEvent m = dataSnapshot.getValue(CheckInEvent.class);
                m.setKey(dataSnapshot.getKey());
                events.add(m);
                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        reference.addChildEventListener(childEventListener);
    }

    private void addMusicEvents() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Events").child("Music");

        ChildEventListener childEventListener=new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                MusicEvent m = dataSnapshot.getValue(MusicEvent.class);
                m.setKey(dataSnapshot.getKey());
                events.add(m);
                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        reference.addChildEventListener(childEventListener);
    }
}
