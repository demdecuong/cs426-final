package com.example.dell.finalcs426;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

public class WebActivity extends AppCompatActivity {

    WebView webView;
    String link;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=this.getIntent();
        link=intent.getStringExtra("link");
        setContentView(R.layout.activity_webview);
        webView=findViewById(R.id.web_view);
        if(link!=null)
        {
            webView.loadUrl(link);
        }
        else
            webView.loadUrl("https://www.youtube.com/watch?v=dQw4w9WgXcQ");

    }
}
