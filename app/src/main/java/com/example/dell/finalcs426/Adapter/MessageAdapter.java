package com.example.dell.finalcs426.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dell.finalcs426.Model.Chat;
import com.example.dell.finalcs426.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    Context context;
    List<Chat> chatList;
    public static int MSG_LEFT = 0;
    public static int MSG_RIGHT = 1;
    String imageUrl;
    FirebaseUser firebaseUser;

    public MessageAdapter(Context context, List<Chat> chatList, String imageUrl) {
        this.context = context;
        this.chatList = chatList;
        this.imageUrl = imageUrl;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        if(i == MSG_LEFT) {
           v = LayoutInflater.from(context).inflate(R.layout.chat_item_left, viewGroup, false);
            return new MessageAdapter.ViewHolder(v);
        }
        else
        {
            v = LayoutInflater.from(context).inflate(R.layout.chat_item_right, viewGroup, false);
            return new MessageAdapter.ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Chat chat = chatList.get(i);

        viewHolder.message.setText(chat.getMsg());

        Picasso.with(context)
                    .load(R.mipmap.ic_launcher)
                    .into(viewHolder.profile_image);
        if(i == chatList.size()-1)
        {
            if(chat.isSeen())
            {
                viewHolder.txt_seen.setText("Seen");
            }else {
                viewHolder.txt_seen.setText("Delivered");
            }
        }else
        {
            viewHolder.txt_seen.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message;
        CircleImageView profile_image;
        TextView txt_seen;

        public ViewHolder(View v) {
            super(v);
            message = v.findViewById(R.id.message);
            profile_image = v.findViewById(R.id.profile_image);
            txt_seen = v.findViewById(R.id.txt_seen);
        }
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        if(chatList.get(position).getSender().equals(firebaseUser.getUid()))
        {
            return MSG_RIGHT;
        }
        return MSG_LEFT;
    }
}
