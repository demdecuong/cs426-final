package com.example.dell.finalcs426;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.dell.finalcs426.Adapter.RewardAdapter;
import com.example.dell.finalcs426.Adapter.UserAdapter;
import com.example.dell.finalcs426.Model.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RewardActivity extends AppCompatActivity {

    TextView txt_countCompleted;
    RecyclerView recyclerView;
    int countCompletedEvent[] ={0};
    FirebaseUser firebaseUser;
    List<Event> eventList = new ArrayList<>();
    RewardAdapter rewardAdapter;
    String TAG = "RewardActivity";
    Event eventTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        //add data
        getCompletedMission();
        recyclerView = findViewById(R.id.recycler_view_reward);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        rewardAdapter = new RewardAdapter(this,eventList);
        recyclerView.setAdapter(rewardAdapter);

        txt_countCompleted = findViewById(R.id.txt_countCompleted);

        countCompletedMission();
    }

    private void getCompletedMission() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("CompletedEvent");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                eventList.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    for(DataSnapshot snapshot1 : snapshot.getChildren())
                    {
                        if(snapshot1.getKey().equals(firebaseUser.getUid()))
                        {
                            addEvent(snapshot.getKey());
                            Log.d(TAG, String.valueOf(eventList.size()));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addEvent(final String key) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Events");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    for(DataSnapshot snapshot1 : snapshot.getChildren())
                    {
                        if(snapshot1.getKey().equals(key))
                        {
                            eventTemp = snapshot1.getValue(Event.class);
                            eventTemp.setKey(snapshot1.getKey());
                            eventList.add(eventTemp);
                            rewardAdapter.notifyDataSetChanged();
                            return;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void countCompletedMission()
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("CompletedEvent");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    for(DataSnapshot snapshot1 : snapshot.getChildren())
                    {
                        if(snapshot1.getKey().equals(firebaseUser.getUid()))
                            countCompletedEvent[0]++;
                    }
                }
                txt_countCompleted.setText(""+countCompletedEvent[0]);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
