package com.example.dell.finalcs426.Model;

public class Chat {
    String sender,receiver,message;
    boolean isSeen = false;

    public Chat(String sender, String receiver, String msg,boolean isSeen) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = msg;
        this.isSeen = isSeen;
    }

    public Chat() {
        this.sender = "";
        this.receiver="";
        this.message="";
        isSeen= false;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String msg) {
        this.message = msg;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSeen() {
        return isSeen;
    }

    public void setSeen(boolean seen) {
        isSeen = seen;
    }
}
