package com.example.dell.finalcs426;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.finalcs426.LoginActivity;
import com.example.dell.finalcs426.MainActivity;
import com.example.dell.finalcs426.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    EditText username,fullname,email,password;
    Button register;
    TextView txt_login;
    ImageView image_qrcode;

    FirebaseAuth auth;
    DatabaseReference reference;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = findViewById(R.id.username);
        fullname = findViewById(R.id.fullname);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        register = findViewById(R.id.register);
        txt_login = findViewById(R.id.txt_login);
        image_qrcode = findViewById(R.id.image_view_qr);


        auth = FirebaseAuth.getInstance();

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd = new ProgressDialog(RegisterActivity.this);
                pd.setMessage("Please wait...");
                pd.show();

                String str_username = username.getText().toString();
                String str_fullname = fullname.getText().toString();
                String str_email = email.getText().toString();
                String str_password = password.getText().toString();

                if(TextUtils.isEmpty(str_username) || TextUtils.isEmpty(str_fullname)|| TextUtils.isEmpty(str_email)
                        || TextUtils.isEmpty(str_password))
                {
                    Toast.makeText(RegisterActivity.this, "All blank need to be filled", Toast.LENGTH_SHORT).show();
                }else if(!isValidPassword(str_password))
                {
                    Toast.makeText(RegisterActivity.this, "Invalid password ", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    register(str_username,str_fullname,str_email,str_password);
                }
            }
        });
    }

    private void register(final String username, final String fullname, final String email, final String password)
    {
        auth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            final String userID = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);

                            HashMap<String,Object> hashMap = new HashMap<>();
                            hashMap.put("id",userID);
                            hashMap.put("username",username.toLowerCase());
                            hashMap.put("fullname",fullname);
                            hashMap.put("bio","");
                            hashMap.put("imageurl","https://firebasestorage.googleapis.com/v0/b/test-agent-mccdln.appspot.com/o/user_2.png?alt=media&token=9c8c8545-95d9-4605-97d9-59f5db2a380b");
                            hashMap.put("level",0);
                            hashMap.put("coin",0);
                            //hashMap.put("qrcodeurl",);

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful())
                                    {
                                        pd.dismiss();
                                        generateQRcode(userID);
                                        Intent intent = new Intent(RegisterActivity.this,DelayActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        Toast.makeText(RegisterActivity.this, "Register successfully !", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }else
                        {
                            pd.dismiss();
                            Toast.makeText(RegisterActivity.this, "You can't register with this email or password", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean isValidPassword(String password) {
        if(password.length() < 6)
        {
            Toast.makeText(this, "Password must have at least 6 characters", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void generateQRcode(String userID) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(userID, BarcodeFormat.QR_CODE,
                    500, 500);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            image_qrcode.setImageBitmap(bitmap);
            Log.d("QR CODE HAS BEEN", " GENERATED");
            pd.dismiss();

          /*  byte[] bytes = ImageManager.getBytesFromBitmap(bitmap,100);
            StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                    .child("users"+"/"+userID+".jpg");
            UploadTask uploadTask = null;
            uploadTask = storageReference.putBytes(bytes);*/

            //Uri barcodeUri = getImageUri(image_qrcode.getContext(),bitmap);
            //

            //uploadImg(userID,username,barcodeUri);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void uploadImg(String userID, EditText username, Uri barcodeUrl) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Uri uri = barcodeUrl;
        StorageReference reference = storageReference.child("users").child(user.getUid()+".jpg");
        reference.putFile(uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(RegisterActivity.this, "Upload Success !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /*
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Log.d("BITMAP HAS BEEN"," CONVERTED INTO URI");
        return Uri.parse(path);
    }*/
}
