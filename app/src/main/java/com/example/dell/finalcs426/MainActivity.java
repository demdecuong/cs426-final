package com.example.dell.finalcs426;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.dell.finalcs426.Fragment.EventFragment;
import com.example.dell.finalcs426.Fragment.FriendsFragment;
import com.example.dell.finalcs426.Fragment.MapFragment;
import com.example.dell.finalcs426.Fragment.ProfileFragment;
import com.example.dell.finalcs426.Fragment.ScanFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    Fragment selectedFragment = null;
    String mapKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent=getIntent();
        mapKey=intent.getStringExtra("key");
        if(mapKey!=null)
        {
            selectedFragment= new MapFragment();
            ((MapFragment) selectedFragment).setStartEventKey(mapKey);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment)
                    .commit();

        }
        else
        {
            selectedFragment=new EventFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment)
                    .commit();

        }


        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListenter);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListenter =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId())
                    {
                        case R.id.nav_event:
                            selectedFragment = null;
                            selectedFragment = new EventFragment();
                            break;
                        case R.id.nav_map:
                            selectedFragment =  new  MapFragment();
                            break;
                        case R.id.nav_scan:
                            selectedFragment = new ScanFragment();
                            break;
                        case R.id.nav_profile:
                            SharedPreferences.Editor editor = getSharedPreferences("PRESS",MODE_PRIVATE).edit();
                            editor.putString("userID", FirebaseAuth.getInstance().getCurrentUser().getUid());
                            editor.apply();
                            selectedFragment = new ProfileFragment();
                            break;
                        case R.id.nav_friends:
                            SharedPreferences.Editor editor2 = getSharedPreferences("PRESS2",MODE_PRIVATE).edit();
                            editor2.putString("userID", FirebaseAuth.getInstance().getCurrentUser().getUid());
                            editor2.apply();
                            selectedFragment = new FriendsFragment();
                            break;
                    }
                    if(selectedFragment!=null)
                    {
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment)
                                .commit();
                    }
                    return true;
                }
            };
}
