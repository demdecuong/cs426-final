package com.example.dell.finalcs426.Model;

import android.content.Context;

public class Event {
    int coin;
    int exp;
    String description;
    String imageUrl;
    String title;
    String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Event() {
    }

    public Event(int coin, int exp, String description, String imageUrl, String title, String key) {
        this.coin = coin;
        this.exp = exp;
        this.description = description;
        this.imageUrl = imageUrl;
        this.title = title;
        this.key = key;
    }

    public Event(int coin, int exp, String decription, String imageUrl, String title) {
        this.coin = coin;
        this.exp = exp;
        this.description = decription;
        this.imageUrl = imageUrl;
        this.title = title;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void doEvent(Context context){}

}
