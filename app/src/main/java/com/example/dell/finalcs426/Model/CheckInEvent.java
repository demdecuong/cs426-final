package com.example.dell.finalcs426.Model;

import android.content.Context;
import android.content.Intent;

import com.example.dell.finalcs426.MainActivity;

public class CheckInEvent extends Event {
    String coordinate;
    String address;

    public CheckInEvent() {
    }

    public CheckInEvent(int coin, int exp, String decription, String imageUrl, String title, String coordinate, String address) {
        super(coin, exp, decription, imageUrl, title);
        this.coordinate = coordinate;
        this.address = address;
    }

    public CheckInEvent(int coin, int exp, String description, String imageUrl, String title, String key, String coordinate, String address) {
        super(coin, exp, description, imageUrl, title, key);
        this.coordinate = coordinate;
        this.address = address;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getDescription() {
        String des;
        des=super.getDescription()+"\n"+address;
        return des;
    }

    @Override
    public void doEvent(Context context) {
        super.doEvent(context);
        Intent intent=new Intent(context, MainActivity.class);
        intent.putExtra("key",key);
        context.startActivity(intent);

    }

}

