package com.example.dell.finalcs426.Model;

import android.content.Context;
import android.content.Intent;
import android.webkit.WebView;

import com.example.dell.finalcs426.WebActivity;

public class MusicEvent extends Event {
    String musicUrl;

    public MusicEvent() {
    }

    public MusicEvent(int coin, int exp, String description, String imageUrl, String title, String key, String musicUrl) {
        super(coin, exp, description, imageUrl, title, key);
        this.musicUrl = musicUrl;
    }

    public MusicEvent(int coin, int exp, String decription, String imageUrl, String title, String musicUrl) {
        super(coin, exp, decription, imageUrl, title);
        this.musicUrl = musicUrl;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }


    @Override
    public void doEvent(Context context) {
        super.doEvent(context);
        Intent intent=new Intent(context, WebActivity.class);
        intent.putExtra("link",musicUrl);
        context.startActivity(intent);
    }
}
