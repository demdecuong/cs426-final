package com.example.dell.finalcs426.Model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.util.List;

public class User {
    private static final String TAG = "User Class";
    String id,email;
    String username,imageUrl,bio;
    Uri barcodeUrl;
    int level=0;
    int coin=0;
    String status;
    int exp = 0;
    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    List<Event>  completedEvent;
    public User()
    {
        username="unknown";
        imageUrl="https://www.google.com/imgres?imgurl=https%3A%2F%2Fe0.365dm.com%2F19%2F07%2F768x432%2Fskysports-cristiano-ronaldo_4732815.jpg%3F20190731184544&imgrefurl=https%3A%2F%2Fwww.skysports.com%2Ffootball%2Fnews%2F11095%2F11774375%2Fcristiano-ronaldo-juventus-reject-k-league-protests-over-friendly-no-show&docid=d9lah_pys-ZlyM&tbnid=aF6ELh16MzbDPM%3A&vet=10ahUKEwj1xfSpyuPjAhUZ4XMBHc2LDVUQMwg9KAAwAA..i&w=768&h=432&bih=667&biw=1366&q=ronaldo&ved=0ahUKEwj1xfSpyuPjAhUZ4XMBHc2LDVUQMwg9KAAwAA&iact=mrc&uact=8";
        level = 1;
        coin = 0;
    }
    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setBarcodeUrl(Uri barcodeUrl) {
        this.barcodeUrl = barcodeUrl;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getBio() {
        return bio;
    }

    public Uri getBarcodeUrl() {
        return barcodeUrl;
    }

    public int getLevel() {
        return level;
    }

    public int getCoin() {
        return coin;
    }

    public User(String id, String email, String username, String imageUrl, String bio, Uri barcodeUrl, int level, int coin) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.imageUrl = imageUrl;
        this.bio = bio;
        this.barcodeUrl = barcodeUrl;
        this.level = level;
        this.coin = coin;
        //  generateQRcode();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void updateLevel()
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        int[] levelList = {100,300,500,700,1000,1200,1500,1900,2500,3000};
        for(int i = 0;i < levelList.length;i++)
        {
            if(getExp()>=levelList[i] && getExp()<levelList[i+1])
            {
                Log.d(TAG,FirebaseAuth.getInstance().getCurrentUser().getUid());
                Log.d(TAG,"level = "+String.valueOf(i+1));
                setLevel(i+1);
                reference.child("level").setValue(i+1);
            }
        }
    }
}
