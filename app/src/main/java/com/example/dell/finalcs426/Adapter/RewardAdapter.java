package com.example.dell.finalcs426.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dell.finalcs426.EventActivity;
import com.example.dell.finalcs426.Model.Event;
import com.example.dell.finalcs426.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RewardAdapter extends RecyclerView.Adapter<RewardAdapter.ViewHolder>{
    private static final String TAG ="RewardAdapter";
    Context context;
    List<Event> eventList;

    public RewardAdapter(Context context, List<Event> eventList) {
        this.context = context;
        this.eventList = eventList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.reward_item,viewGroup,false);
        return new RewardAdapter.ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Event event = eventList.get(i);
        /*Picasso.with(context)
                .load(event.getImageUrl())
                .into(viewHolder.image_profile);*/
        viewHolder.txt_title.setText(event.getTitle());
        viewHolder.txt_exp.setText(String.valueOf(event.getExp())+" exp");
        viewHolder.txt_coin.setText(String.valueOf(event.getCoin())+" coin");
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(context, EventActivity.class);
                intent.putExtra("event_key",event.getKey());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image_profile;
        TextView txt_exp,txt_coin,txt_title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image_profile = itemView.findViewById(R.id.image_profile);
            txt_coin= itemView.findViewById(R.id.txt_coin);
            txt_exp = itemView.findViewById(R.id.txt_exp);
            txt_title = itemView.findViewById(R.id.txt_title);
        }
    }
}
